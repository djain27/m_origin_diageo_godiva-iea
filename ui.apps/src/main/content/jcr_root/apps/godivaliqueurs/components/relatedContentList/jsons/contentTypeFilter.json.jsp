<%--

  <contentType.json.jsp>

  Copyright 2013 IEA, SapientNitro
  All Rights Reserved.

  This software is the confidential and proprietary information of
  SapientNitro, ("Confidential Information"). You shall not
  disclose such Confidential Information and shall use it only in
  accordance with the terms of the license agreement you entered into
  with SapientNitro.

  ==============================================================================

  This will return the json for content type dropdown.

  ==============================================================================

--%>
<%@ include file="/libs/foundation/global.jsp" %>

[

{
 "text":"Primary Image of page",
 "value":"isImageDetail"
 },
{
 "text":"Page Title Headline copy",
 "value":"isPageTitle"
 },
{
 "text":"CTA URL for the source page",
 "value":"isCtaUrl"
 }

]