/*******************************************************************************
 * Copyright (c) 2015 Diageo.
 *
 * All rights reserved. Do not distribute any of these files without prior consent from Diageo.
 *
 * Contributors:
 *     Publicis.Sapient - initial API and implementation
 *******************************************************************************/
package com.diageo.godivaliqueurs.viewhelper.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingConstants;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.diageo.godivaliqueurs.components.GodivaliqueursComponents;
import com.diageo.platform.foundation.viewhelper.base.BaseViewHelper;
import com.sapient.platform.iea.aem.core.constants.CommonConstants;
import com.sapient.platform.iea.aem.core.viewhelper.api.ViewHelper;
import com.diageo.platform.foundation.utils.ImagePropertiesUtil;

import org.apache.sling.api.resource.Resource;

/**
 * The Class RecipeViewHelperImpl Responsible for reading manual author input for Recipe component..
 */
@Component(description = "GodivaliqueursSampleViewHelperImpl.java", immediate = true, metatype = true, label = "GodivaliqueursSampleViewHelperImpl")
@Service(value = { GodivaliqueursSampleViewHelperImpl.class, ViewHelper.class })
@Properties({
@Property(name = SlingConstants.PROPERTY_RESOURCE_TYPE, value = GodivaliqueursComponents.SAMPLE, propertyPrivate = true),
		@Property(name = Constants.SERVICE_RANKING, intValue = CommonConstants.DEFAULT_SERVCE_RANKING, propertyPrivate = true) })

public class GodivaliqueursSampleViewHelperImpl extends BaseViewHelper {

	/**
	 * Constant for RESOURCE_RESOLVER .
	 */
	private static final String RESOURCE_RESOLVER = "RESOURCE_RESOLVER";
	
	/**
	 * Constant for LOGGER .
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(GodivaliqueursSampleViewHelperImpl.class);

	ResourceResolver resourceResolver = null;

	/**
	 * Process data.
	 *
	 * @param content
	 *            the content
	 * @param resources
	 *            the resources
	 * @return the map
	 */

	public Map<String, Object> processData(final Map<String, Object> content, final Map<String, Object> resources) {
		
		LOGGER.info("GodivaliqueursSampleViewHelper #processData:Entry");
		Map<String, Object> map = new HashMap<String, Object>(
				CommonConstants.ARRAY_INIT * CommonConstants.ARRAY_INIT);
		String jsonNameSpace = MapUtils.getString(content, "jsonNameSpace");
		resourceResolver = (ResourceResolver) resources.get(RESOURCE_RESOLVER);
		
		/*code for populating the properties map is to be written here
		 *  
		 */
		
		LOGGER.debug("GodivaliqueursSampleViewHelper #processData:Exit" + map);
		return map;
		
	}

	
}
